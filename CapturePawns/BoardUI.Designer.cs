﻿namespace CapturePawns
{
    partial class BoardUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.board = new System.Windows.Forms.TableLayoutPanel();
            this.knight_picturebox = new System.Windows.Forms.PictureBox();
            this.pawn1_picturebox = new System.Windows.Forms.PictureBox();
            this.pawn2_picturebox = new System.Windows.Forms.PictureBox();
            this.pawn3_picturebox = new System.Windows.Forms.PictureBox();
            this.pawn4_picturebox = new System.Windows.Forms.PictureBox();
            this.pawn5_picturebox = new System.Windows.Forms.PictureBox();
            this.go_stop_button = new System.Windows.Forms.Button();
            this.selection_comboBox = new System.Windows.Forms.ComboBox();
            this.reset_button = new System.Windows.Forms.Button();
            this.board.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.knight_picturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn1_picturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn2_picturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn3_picturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn4_picturebox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn5_picturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // board
            // 
            this.board.BackColor = System.Drawing.Color.Transparent;
            this.board.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.board.ColumnCount = 20;
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.Controls.Add(this.knight_picturebox, 12, 4);
            this.board.Controls.Add(this.pawn1_picturebox, 9, 6);
            this.board.Controls.Add(this.pawn2_picturebox, 4, 4);
            this.board.Controls.Add(this.pawn3_picturebox, 14, 8);
            this.board.Controls.Add(this.pawn4_picturebox, 12, 9);
            this.board.Controls.Add(this.pawn5_picturebox, 2, 3);
            this.board.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.board.Location = new System.Drawing.Point(12, 10);
            this.board.Margin = new System.Windows.Forms.Padding(0);
            this.board.Name = "board";
            this.board.RowCount = 20;
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.board.Size = new System.Drawing.Size(644, 644);
            this.board.TabIndex = 0;
            // 
            // knight_picturebox
            // 
            this.knight_picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.knight_picturebox.Image = global::CapturePawns.Properties.Resources.knight;
            this.knight_picturebox.InitialImage = null;
            this.knight_picturebox.Location = new System.Drawing.Point(386, 130);
            this.knight_picturebox.Margin = new System.Windows.Forms.Padding(0);
            this.knight_picturebox.Name = "knight_picturebox";
            this.knight_picturebox.Size = new System.Drawing.Size(30, 30);
            this.knight_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.knight_picturebox.TabIndex = 0;
            this.knight_picturebox.TabStop = false;
            // 
            // pawn1_picturebox
            // 
            this.pawn1_picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pawn1_picturebox.Image = global::CapturePawns.Properties.Resources.pawn;
            this.pawn1_picturebox.Location = new System.Drawing.Point(290, 194);
            this.pawn1_picturebox.Margin = new System.Windows.Forms.Padding(0);
            this.pawn1_picturebox.Name = "pawn1_picturebox";
            this.pawn1_picturebox.Size = new System.Drawing.Size(30, 30);
            this.pawn1_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pawn1_picturebox.TabIndex = 1;
            this.pawn1_picturebox.TabStop = false;
            // 
            // pawn2_picturebox
            // 
            this.pawn2_picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pawn2_picturebox.Image = global::CapturePawns.Properties.Resources.pawn;
            this.pawn2_picturebox.Location = new System.Drawing.Point(130, 130);
            this.pawn2_picturebox.Margin = new System.Windows.Forms.Padding(0);
            this.pawn2_picturebox.Name = "pawn2_picturebox";
            this.pawn2_picturebox.Size = new System.Drawing.Size(30, 30);
            this.pawn2_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pawn2_picturebox.TabIndex = 2;
            this.pawn2_picturebox.TabStop = false;
            // 
            // pawn3_picturebox
            // 
            this.pawn3_picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pawn3_picturebox.Image = global::CapturePawns.Properties.Resources.pawn;
            this.pawn3_picturebox.Location = new System.Drawing.Point(450, 258);
            this.pawn3_picturebox.Margin = new System.Windows.Forms.Padding(0);
            this.pawn3_picturebox.Name = "pawn3_picturebox";
            this.pawn3_picturebox.Size = new System.Drawing.Size(30, 30);
            this.pawn3_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pawn3_picturebox.TabIndex = 3;
            this.pawn3_picturebox.TabStop = false;
            // 
            // pawn4_picturebox
            // 
            this.pawn4_picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pawn4_picturebox.Image = global::CapturePawns.Properties.Resources.pawn;
            this.pawn4_picturebox.Location = new System.Drawing.Point(386, 290);
            this.pawn4_picturebox.Margin = new System.Windows.Forms.Padding(0);
            this.pawn4_picturebox.Name = "pawn4_picturebox";
            this.pawn4_picturebox.Size = new System.Drawing.Size(30, 30);
            this.pawn4_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pawn4_picturebox.TabIndex = 4;
            this.pawn4_picturebox.TabStop = false;
            // 
            // pawn5_picturebox
            // 
            this.pawn5_picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pawn5_picturebox.ErrorImage = null;
            this.pawn5_picturebox.Image = global::CapturePawns.Properties.Resources.pawn;
            this.pawn5_picturebox.InitialImage = null;
            this.pawn5_picturebox.Location = new System.Drawing.Point(66, 98);
            this.pawn5_picturebox.Margin = new System.Windows.Forms.Padding(0);
            this.pawn5_picturebox.Name = "pawn5_picturebox";
            this.pawn5_picturebox.Size = new System.Drawing.Size(30, 30);
            this.pawn5_picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pawn5_picturebox.TabIndex = 5;
            this.pawn5_picturebox.TabStop = false;
            // 
            // go_stop_button
            // 
            this.go_stop_button.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.go_stop_button.ForeColor = System.Drawing.Color.ForestGreen;
            this.go_stop_button.Location = new System.Drawing.Point(669, 252);
            this.go_stop_button.Name = "go_stop_button";
            this.go_stop_button.Size = new System.Drawing.Size(107, 33);
            this.go_stop_button.TabIndex = 1;
            this.go_stop_button.Text = "GO!";
            this.go_stop_button.UseVisualStyleBackColor = true;
            this.go_stop_button.Click += new System.EventHandler(this.go_stop_button_Click);
            // 
            // selection_comboBox
            // 
            this.selection_comboBox.FormattingEnabled = true;
            this.selection_comboBox.Items.AddRange(new object[] {
            "BFS",
            "DFS",
            "A*-1",
            "A*-2"});
            this.selection_comboBox.Location = new System.Drawing.Point(669, 213);
            this.selection_comboBox.Name = "selection_comboBox";
            this.selection_comboBox.Size = new System.Drawing.Size(107, 21);
            this.selection_comboBox.TabIndex = 2;
            // 
            // reset_button
            // 
            this.reset_button.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reset_button.ForeColor = System.Drawing.Color.ForestGreen;
            this.reset_button.Location = new System.Drawing.Point(669, 321);
            this.reset_button.Name = "reset_button";
            this.reset_button.Size = new System.Drawing.Size(107, 33);
            this.reset_button.TabIndex = 3;
            this.reset_button.Text = "Reset";
            this.reset_button.UseVisualStyleBackColor = true;
            // 
            // BoardUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 665);
            this.Controls.Add(this.reset_button);
            this.Controls.Add(this.selection_comboBox);
            this.Controls.Add(this.go_stop_button);
            this.Controls.Add(this.board);
            this.Name = "BoardUI";
            this.Text = "Capture All Pawns";
            this.Load += new System.EventHandler(this.BoardUI_Load);
            this.board.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.knight_picturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn1_picturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn2_picturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn3_picturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn4_picturebox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pawn5_picturebox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel board;
        private System.Windows.Forms.Button go_stop_button;
        private System.Windows.Forms.PictureBox knight_picturebox;
        private System.Windows.Forms.PictureBox pawn1_picturebox;
        private System.Windows.Forms.PictureBox pawn2_picturebox;
        private System.Windows.Forms.PictureBox pawn3_picturebox;
        private System.Windows.Forms.PictureBox pawn4_picturebox;
        private System.Windows.Forms.ComboBox selection_comboBox;
        private System.Windows.Forms.PictureBox pawn5_picturebox;
        private System.Windows.Forms.Button reset_button;
    }
}


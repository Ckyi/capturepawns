﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Windows.Threading;

namespace CapturePawns
{
    public partial class BoardUI : Form
    {
        private AI aiLogic;
        private int pieceCount = 6;
        private List<PictureBox> picBoxList;

        public BoardUI()
        {
            InitializeComponent();
        }

        private void prepareDisplay()
        {
            this.aiLogic = new AI(this, pieceCount);

            State root = this.aiLogic.getInitialState();
            List<Position> temp = new List<Position>();
            temp.Add(root.knight);
            foreach (Position p in root.pawns)
            {
                temp.Add(p);
            }
            
            this.update(temp[0], temp.Skip(1).ToList());            
        }

        private void BoardUI_Load(object sender, EventArgs e)
        {
            this.board.CellPaint += new TableLayoutCellPaintEventHandler(board_CellPaint);
            this.selection_comboBox.SelectedIndex = Constants.BFS;
            this.picBoxList = new List<PictureBox>();
            this.addPictureboxToList();
            this.prepareDisplay();
    
        }


        void board_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Row == 0 || e.Row == 19 || e.Column == 0 || e.Column == 19)
            {
                Graphics g = e.Graphics;
                Rectangle r = e.CellBounds;
                g.FillRectangle(Brushes.Gray, r);
            }
        }

        
        public void update(Position knight, List<Position> pawns)                
        { 
            // Remove from old positions
            /*this.board.Controls.Remove(pawn1_picturebox);
            this.board.Controls.Remove(pawn2_picturebox);
            this.board.Controls.Remove(pawn3_picturebox);
            this.board.Controls.Remove(pawn4_picturebox);
            this.board.Controls.Remove(pawn5_picturebox);
            this.board.Controls.Remove(knight_picturebox);*/

            /*pawn1_picturebox.Visible = false;
            pawn2_picturebox.Visible = false;
            pawn3_picturebox.Visible = false;
            pawn4_picturebox.Visible = false;
            pawn5_picturebox.Visible = false;*/
         
            // Load images in new positions            
            //this.board.Controls.Remove(knight_picturebox);
            this.board.Controls.Add(knight_picturebox, knight.col, knight.row);
           
            for(int i=0; i < pawns.Count; i++)
            { 
                //this.board.Controls.Add(this.picBoxList[i], pawns[i].col, pawns[i].row);
                this.picBoxList[i].WaitOnLoad = true;
                //this.board.Controls.Remove(this.picBoxList[i]);
                this.board.Controls.Add(this.picBoxList[i], pawns[i].col, pawns[i].row);
                
            }
            
            
            
        }

        private void addPictureboxToList()
        {            
            this.picBoxList.Add(pawn1_picturebox);
            this.picBoxList.Add(pawn2_picturebox);
            this.picBoxList.Add(pawn3_picturebox);
            this.picBoxList.Add(pawn4_picturebox);
            this.picBoxList.Add(pawn5_picturebox);
        }

        public void removeAllPicturebox()
        {
            this.board.Controls.Remove(pawn1_picturebox);
            this.board.Controls.Remove(pawn2_picturebox);
            this.board.Controls.Remove(pawn3_picturebox);
            this.board.Controls.Remove(pawn4_picturebox);
            this.board.Controls.Remove(pawn5_picturebox);
            this.board.Controls.Remove(knight_picturebox);
        }

        public void update2()
        { 
        
        
        }

        private void go_stop_button_Click(object sender, EventArgs e)
        {
            State root = null;
            int searchAlgorithm = this.selection_comboBox.SelectedIndex;

            switch(((Button)sender).Text)
            {
                case "GO!":
                    root = this.aiLogic.startGame(Constants.START,searchAlgorithm);
                    break;
                case "STOP!":
                    root = this.aiLogic.startGame(Constants.STOP,1);
                    break;
                case "RESET":
                    root = this.aiLogic.startGame(Constants.RESET,1);
                    break;           
            }

            this.replay(root);            
            
        }

        private void replay(State currentState)
        {            
            DispatcherTimer timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };
            timer.Tick += (o, e) =>
            {
                List<Position> pos = new List<Position>();

                foreach(Position p in currentState.pawns)
                {
                    pos.Add(p);
                }
                this.board.SuspendLayout();
                this.update(currentState.knight, pos);
                this.board.ResumeLayout();

                currentState = currentState.next;
                if (currentState == null)
                {
                    timer.IsEnabled = false;
                    //this.prepareDisplay();
                }
            };
            timer.IsEnabled = true;
        
        
        }


 
    }
}

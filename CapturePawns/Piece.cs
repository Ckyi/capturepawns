﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturePawns
{
    abstract class Piece
    {
        protected Position position; 
        
        abstract protected bool isMoveValid(Position move);
        
        public Position getCurrentPosition()
        {
            return this.position;
        }
        
    }
}

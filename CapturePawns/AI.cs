﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace CapturePawns
{
    class AI
    {
        private Form ui;
        private int pieceCount; 
        private State root;
        private Knight knight;
        private List<Pawn> pawns;
        private List<Position> pawnPos1;
        private List<Position> pawnPos2;
        private int index = 0;
        private HashSet<String> visitedState;
        private Queue bfsFringe;
        private Stack dfsFringe;
        private State aStarHOneState;

        public AI(Form argui, int argPieceCount)
        {
            this.ui = argui;
            this.pieceCount = argPieceCount;            
            this.generateInitialState();
            
            /*this.knight = new Knight(this.root.knightLocation);
            this.pawns = new List<Pawn>();

            foreach (Position pos in this.root.pawnsLocation)
            {
                pawns.Add(new Pawn(pos));
            }*/

                    

        }


        public State getInitialState()
        {
            return this.root;            
        }

        public State startGame(int buttonState, int searchAlgorithm)
        {          
            
            if (searchAlgorithm == Constants.BFS)
            {
                this.findPathBFS();
            }
            else if (searchAlgorithm == Constants.DFS)
            {
                this.findPathDFS();
            }
            else if (searchAlgorithm == Constants.A_STARHONE)
            {
                this.findAStarHOne();
            }
            else
            { 
            }
                
            

            
            
            


            return this.root;
            //Generate new initial positions after each game
            //clear the pawn list before you start the next game otherwise you'll see pawns from the previous game
        }

        private void findPathDFS()
        {
            this.dfsFringe = new Stack();
            State currentState;

            this.dfsFringe.Push(this.root);

            do
            {
                currentState = (State)(this.dfsFringe.Pop());

                //this.visitedState.Add(currentState.ToString());

                if (currentState.pawnCount == 0)
                {
                    this.updateNextPointer(currentState);
                    break;
                }

                this.generateNewStatesDFS(currentState);
                Console.WriteLine(index++);
            } while (this.dfsFringe.Count > 0);
        }

        private void findAStarHOne()
        {
            this.generateNewStatesAStarHOne(this.root);

            do
            {
                this.generateNewStatesAStarHOne(aStarHOneState);
                Console.WriteLine(index++);
            } while (aStarHOneState.pawnCount > 0);

            if (aStarHOneState.pawnCount == 0)
            {
                this.updateNextPointer(aStarHOneState); // Update next pointer of all nodes on the goal path so that you can go from Root to goal
             
            }
            
        }

        // Probably error in generating new moves for knight. Queue doesnt reflect 8 moves
        private void findPathBFS()
        {            
            this.bfsFringe = new Queue();            
            State currentState;            
            
            this.bfsFringe.Enqueue(this.root);

            do
            {
                currentState = (State)(this.bfsFringe.Dequeue());

                //this.visitedState.Add(currentState.ToString()); // Add it to the list

                if (currentState.pawnCount == 0)
                {
                    this.updateNextPointer(currentState); // Update next pointer of all nodes on the goal path so that you can go from Root to goal
                    break; 
                }


                this.generateNewStatesBFS(currentState);
                
                Console.WriteLine(index++);
            } while (this.bfsFringe.Count > 0);
            
        }

       

        // Update Next pointer
        private void updateNextPointer(State goalState)
        {
            State temp;
            while (goalState.previous != null)
            { 
                temp = goalState.previous;
                temp.next = goalState;
                goalState = temp;
            }        
        }

        // Generate new states DFS
        private void generateNewStatesDFS(State currentState)
        {   
            List<Position> nextPos;
            List<Position> tempPosList;
            State temp;
            int pawnCount;

            knight.move(currentState.knight);
            List<Position> knightValidpositions = knight.getAllValidMoves();

            if (currentState.positionArray == Constants.POSITION_ONE)
            {
                tempPosList = pawnPos2;
            }
            else
                tempPosList = pawnPos1;

            Position deadPawn = new Position(0, 0);
            String positionStr;

            foreach (Position knightPosition in knightValidpositions)
            {
                nextPos = new List<Position>();
                pawnCount = currentState.pawnCount;
                positionStr = knightPosition.ToString() + ",";

                for (int i = 0; i < currentState.pawns.Count; i++)
                {
                    if (currentState.pawns[i] == deadPawn)
                        nextPos.Add(deadPawn);
                    else if ((currentState.pawns[i] == knightPosition) || (tempPosList[i] == knightPosition))
                    {
                        nextPos.Add(deadPawn);
                        pawnCount--;
                    }
                    else
                        nextPos.Add(tempPosList[i]);

                    positionStr += nextPos[i].ToString() + ",";
                }

                temp = new State(knightPosition, nextPos, pawnCount, currentState, null, (-1) * currentState.positionArray, positionStr); //position2 array is -1


                if (!this.visitedState.Contains(positionStr)) { 
                    this.dfsFringe.Push(temp);
                    this.visitedState.Add(temp.ToString());
                }
            }
        
        }


        // Generate new states BFS
        private void generateNewStatesBFS(State currentState)
        {
            // First move the knight to the position stored in the currentState
            // Then get all valid moves of knight from that position                        
            knight.move(currentState.knight);
            List<Position> knightValidpositions = knight.getAllValidMoves();                        

            List<Position> nextPos;
            List<Position> tempPosList;
            State temp;            
            int pawnCount;


            /*
            int m = 0;
            for (int i = 0; i < pawnPos1.Count; i++)
            {
                if (pawnPos1[i] == currentState.pawns[m])
                {
                    position1 = true;
                    tempPosList.Add(pawnPos1[i]); //change it back to pawnPos2                    
                    m++;
                    if (currentState.pawns.Count == m) break;
                }
            }

            if (!position1)
            {
                m = 0;
                for (int i = 0; i < pawnPos2.Count; i++)
                {
                    if (pawnPos2[i] == currentState.pawns[m])
                    {
                        tempPosList.Add(pawnPos1[i]);
                        m++;
                        if (currentState.pawns.Count == m) break;
                    }
                }
            }*/

            if (currentState.positionArray == Constants.POSITION_ONE)
            {
                tempPosList = pawnPos2;
            }
            else
                tempPosList = pawnPos1;
                                
            Position deadPawn = new Position(0,0);
            String positionStr;

            foreach (Position knightPosition in knightValidpositions)
            {
                nextPos = new List<Position>();
                pawnCount = currentState.pawnCount;
                positionStr = knightPosition.ToString() + ",";

                for(int i=0;i < currentState.pawns.Count; i++)
                {
                    if (currentState.pawns[i] == deadPawn)
                        nextPos.Add(deadPawn);
                    else if ((currentState.pawns[i] == knightPosition) || (tempPosList[i] == knightPosition))
                    {
                        nextPos.Add(deadPawn);
                        pawnCount--;
                    }                    
                    else
                        nextPos.Add(tempPosList[i]);

                    positionStr += nextPos[i].ToString() + ",";
                }

                temp = new State(knightPosition, nextPos, pawnCount, currentState, null,(-1) * currentState.positionArray,positionStr); //position2 array is -1


                if (!this.visitedState.Contains(positionStr))
                {
                    this.bfsFringe.Enqueue(temp);
                    this.visitedState.Add(temp.ToString()); 
                }
                
                /*visited = false;
                
                for (int k = 0; k < this.visitedState.Count; k++)
                {   
                    if (temp == this.visitedState[k])
                    {
                        visited = true;
                        break;
                    }
                }

                if (!visited)
                    this.fringe.Enqueue(temp);*/

            }


          




            /*

            if (currentState.turn == Constants.KNIGHT_TURN)
            {
                

                nextPos = new List<Position>();

                foreach (Position ppos in tempPosList)
                {
                    if (currentState.knight != ppos) nextPos.Add(ppos);
                }

                temp = new State(currentState.knight, nextPos, nextPos.Count, currentState, null,Constants.PAWN_TURN);
                visited = false;

                for (int k = 0; k < this.visitedState.Count; k++)
                {
                    if (temp == this.visitedState[k])
                    {
                        visited = true;
                        break;
                    }
                }

                if (!visited)
                    this.fringe.Enqueue(temp);
            }
            else { 
                knightValidpositions = knight.getAllValidMoves();

                
            }
                     
            foreach(Position knightPosition in knightValidpositions)
            {
                nextPos = new List<Position>();                
               
                //Check if knight and any pawn overlap position
                for (int i = 0; i < tempPosList.Count; i++)
                {
                    if(knightPosition != tempPosList[i])
                    {
                        nextPos.Add(tempPosList[i]);
                    }
                    
                        
                }
                
                int x;
                if (nextPos.Count == 0)
                    x = 1;
                // Check if the state is visited already
                temp = new State(knightPosition, nextPos, nextPos.Count, currentState, null);
                visited = false;

                for (int k = 0; k < this.visitedState.Count; k++)
                {   
                    if (temp == this.visitedState[k]) 
                    {
                        visited = true;
                        break;
                    }
                }

                 if (!visited) this.fringe.Enqueue(temp);
                    
            }*/
        }

        private void generateNewStatesAStarHOne(State currentState)
        {
            List<Position> nextPos;
            List<Position> tempPosList;
            State temp;
            State temp2 = null;
            int pawnCount;
            int distance = 0;

            knight.move(currentState.knight);
            List<Position> knightValidpositions = knight.getAllValidMoves();

            if (currentState.positionArray == Constants.POSITION_ONE)
            {
                tempPosList = pawnPos2;
            }
            else
                tempPosList = pawnPos1;

            Position deadPawn = new Position(0, 0);
            String positionStr;

            foreach (Position knightPosition in knightValidpositions)
            {
                nextPos = new List<Position>();
                pawnCount = currentState.pawnCount;
                positionStr = knightPosition.ToString() + ",";
                distance = -1;

                for (int i = 0; i < currentState.pawns.Count; i++)
                {
                    if (currentState.pawns[i] == deadPawn)
                        nextPos.Add(deadPawn);
                    else if ((currentState.pawns[i] == knightPosition) || (tempPosList[i] == knightPosition))
                    {
                        nextPos.Add(deadPawn);
                        pawnCount--;
                        distance = 0;
                    }
                    else
                    {
                        // Store the distance to the closest pawn
                        if (distance == -1) {
                            distance = (Math.Abs(knightPosition.row - tempPosList[i].row)
                             + Math.Abs(knightPosition.col - tempPosList[i].col));
                        }
                        else if(distance >  ((Math.Abs(knightPosition.row - tempPosList[i].row) +
                          Math.Abs(knightPosition.col - tempPosList[i].col)))){
                            distance = (Math.Abs(knightPosition.row - tempPosList[i].row)
                             + Math.Abs(knightPosition.col - tempPosList[i].col));
                        }
                        nextPos.Add(tempPosList[i]);
                    }
                    positionStr += nextPos[i].ToString() + ",";
                }

                temp = new State(knightPosition, nextPos, pawnCount, currentState, null, (-1) * currentState.positionArray, positionStr); //position2 array is -1
                temp.closestDistance = distance;
                
                if(this.visitedState.Contains(temp.ToString())) continue;

                if(temp2 == null) temp2 = temp;
                else 
                {                    
                    if(temp2.closestDistance > temp.closestDistance) 
                        temp2 = temp;                    
                }
               
            }

            
            this.visitedState.Add(temp2.ToString());
            

            this.aStarHOneState = temp2;
            
            
        
        }

        // Generate random positions for pieces
        private void generateInitialState()
        {
            pawnPos1 = new List<Position>();
            pawnPos2 = new List<Position>();            
            knight = new Knight();
            pawns = new List<Pawn>();

            /*for (int i = 0; i < this.pieceCount - 1; i++)
            {
                this.pawns.Add(new Pawn());
            } */
            List<Position> occupiedPosition = new List<Position>();
            Random rnd = new Random();
            int row, col;
            Position temp;            
            String positionStr = "";

            for (int i = 0; i < this.pieceCount; i++) 
            {
                row = rnd.Next(Constants.BOARD_TOP, Constants.BOARD_BOTTOM); 
                col = rnd.Next(Constants.BOARD_LEFT, Constants.BOARD_RIGHT); 

                temp = new Position(row, col);

                if (isSamePosition(occupiedPosition, temp)) i--;
                else {
                    if (i == 0) knight.move(temp);
                    else {
                        this.pawns.Add(new Pawn(temp));
                        pawnPos1.Add(temp);
                    }
                    occupiedPosition.Add(temp);
                    positionStr += temp.ToString() + ",";
                    
                }
                
            }            
            this.root = new State(knight.getCurrentPosition(), pawnPos1, pawnPos1.Count, null, null,Constants.POSITION_ONE,positionStr); //getcurrentposition returns protected attribute. check if it copies by reference                        
            this.visitedState = new HashSet<String>();    
            //Move pawns to the next positions
            foreach (Pawn p in pawns)
            {
                p.move();
                pawnPos2.Add(p.getCurrentPosition());
            }
        }

        // Check if given piece location overlaps with anyone of the existing pieces
        private bool isSamePosition(List<Position> posList, Position pos) 
        {
            foreach(Position item in posList)
            {
                if ((item.row == pos.row) && (item.col == pos.col)) return true;
                if ((item.row == pos.row + 1 && item.col == pos.col) ||
                    (item.row == pos.row - 1 && item.col == pos.col) ||
                    (item.row == pos.row && item.col == pos.col + 1) ||
                    (item.row == pos.row && item.col == pos.col - 1) ||
                    (item.row == pos.row + 1 && item.col == pos.col - 1) ||
                    (item.row == pos.row + 1 && item.col == pos.col + 1) ||
                    (item.row == pos.row - 1 && item.col == pos.col - 1) ||
                    (item.row == pos.row - 1 && item.col == pos.col + 1))
                    return true;
            }
            
            return false;
        }

        
        
    }
}

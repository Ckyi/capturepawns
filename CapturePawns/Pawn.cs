﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturePawns
{
    class Pawn : Piece
    {       
        private Position nextMove;
        private Position orgPosition;        

        /*public Pawn(Position argPosition)
        {
            this.position = argPosition;
            this.orgPosition = argPosition;
            this.setNextMove();
        }*/

        public Pawn() {}

        public Pawn(Position argPosition) {
            this.initializeAllAttributes(argPosition);
        }

        public void initializeAllAttributes(Position argPosition)
        {
            this.position = argPosition; //Struct does not need new
            this.orgPosition = argPosition;
            this.setNextMove(); //Fix so that two pawns do not overlap
        }

        public void setNextMove()
        {            
            Position temp;
            List<Position> newPositions = new List<Position>();            
            Random rnd = new Random();

            temp.row = this.position.row + Constants.EAST.row;
            temp.col = this.position.col + Constants.EAST.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp); 

            temp.row = this.position.row + Constants.WEST.row;
            temp.col = this.position.col + Constants.WEST.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp); 
            
            temp.row = this.position.row + Constants.NORTH.row;
            temp.col = this.position.col + Constants.NORTH.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp); 

            temp.row = this.position.row + Constants.SOUTH.row;
            temp.col = this.position.col + Constants.SOUTH.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp); 

            int index = rnd.Next(0,newPositions.Count()-1);
            this.nextMove = newPositions[index];
        }

        public void move()
        {
            if (this.position == this.orgPosition) this.position = this.nextMove;
            else this.position = this.orgPosition;
        }


        protected override bool isMoveValid(Position move)
        {
            if ((move.row > Constants.BOARD_BOTTOM) ||
               (move.row < Constants.BOARD_TOP) ||
               (move.col > Constants.BOARD_RIGHT) ||
               (move.col < Constants.BOARD_LEFT)) {
                   return false;
            }

            

            return true;   
        
        }

    }
}

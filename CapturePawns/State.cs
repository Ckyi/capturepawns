﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturePawns
{
    class State
    {
        public int pawnCount;

        public State previous;

        public State next;        

        public List<Position> pawns;

        public Position knight;

        public int positionArray;

        private String positionStr;

        public int closestDistance;

        public State(Position knightPos, List<Position> pawnPos, int argPawnCount, State argPrev, State argNext, int argPositionArray, String argPositionStr)
        {
            this.knight = knightPos; //no need for new since Position is struct
            this.pawnCount = argPawnCount;
            this.previous = argPrev;
            this.next = argNext;
            this.pawns = pawnPos;
            this.positionArray = argPositionArray;
            this.positionStr = argPositionStr;
        }

        public override string ToString()
        {
            return this.positionStr;
        }

        
        public static bool operator ==(State s1, State s2)
        {
            if (object.ReferenceEquals(s1, null))
            {
                return object.ReferenceEquals(s2, null);
            }
            else {
                if (object.ReferenceEquals(s2, null)) return false;
            } 

            if (s1.pawnCount != s2.pawnCount) 
                return false;
            if (s1.knight != s2.knight) 
                return false;
            for (int i = 0; i < s1.pawnCount; i++)
            {
                if (s1.pawns[i] != s2.pawns[i]) 
                    return false; 
            }

            return true;    
        }

        public static bool operator !=(State s1, State s2)
        {
            return !(s1 == s2);
        }
    }
}

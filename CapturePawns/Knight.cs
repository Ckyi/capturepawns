﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturePawns
{
    class Knight : Piece
    {        

        public Knight(Position argPosition)
        {
            this.position = argPosition;
        }

        public Knight(){ }
        

        public List<Position> getAllValidMoves()
        {
            Position temp;            
            List<Position> newPositions = new List<Position>();            

            // East Top position
            temp.row = this.position.row + Constants.EAST_TOP.row;
            temp.col = this.position.col + Constants.EAST_TOP.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // East Bottom position
            temp.row = this.position.row + Constants.EAST_BOTTOM.row;
            temp.col = this.position.col + Constants.EAST_BOTTOM.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // West Top position
            temp.row = this.position.row + Constants.WEST_TOP.row;
            temp.col = this.position.col + Constants.WEST_TOP.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // West Bottom position
            temp.row = this.position.row + Constants.WEST_BOTTOM.row;
            temp.col = this.position.col + Constants.WEST_BOTTOM.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // North Left position
            temp.row = this.position.row + Constants.NORTH_LEFT.row;
            temp.col = this.position.col + Constants.NORTH_LEFT.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // North Right position
            temp.row = this.position.row + Constants.NORTH_RIGHT.row;
            temp.col = this.position.col + Constants.NORTH_RIGHT.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // South Left position
            temp.row = this.position.row + Constants.SOUTH_LEFT.row;
            temp.col = this.position.col + Constants.SOUTH_LEFT.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            // South Right position
            temp.row = this.position.row + Constants.SOUTH_RIGHT.row;
            temp.col = this.position.col + Constants.SOUTH_RIGHT.col;
            if (this.isMoveValid(temp)) newPositions.Add(temp);

            return newPositions;
        }

        protected override bool isMoveValid(Position move)
        {
            if ((move.row > Constants.BOARD_BOTTOM) ||
               (move.row < Constants.BOARD_TOP) ||
               (move.col > Constants.BOARD_RIGHT) ||
               (move.col < Constants.BOARD_LEFT)) {
                   return false;
            }

            return true;
        }

        public void move(Position move)
        {
            this.position = move;
        }

    }
}

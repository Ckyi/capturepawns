﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturePawns
{
    internal static class Constants
    {
        internal static int START { get { return 1; } }
        internal static int STOP { get { return 2; } }
        internal static int RESET { get { return 3; } }

        internal static int BFS { get { return 0; } }
        internal static int DFS { get { return 1; } }
        internal static int A_STARHONE { get { return 2; } }
        internal static int A_STARHTWO { get { return 3; } }

        internal static int BOARD_TOP { get{ return 1;}}
        internal static int BOARD_LEFT { get { return 1; } }
        //internal static int BOARD_BOTTOM { get { return 18; } }
        //internal static int BOARD_RIGHT { get { return 18; } }
        internal static int BOARD_BOTTOM { get { return 18; } }
        internal static int BOARD_RIGHT { get { return 18; } }

        internal static int POSITION_ONE = 1;
        internal static int POSITION_TWO = -1;

        internal static int KNIGHT_HOP = 3;
        


        internal static Position WEST
        {
            get
            {
                Position W;
                W.col = -1;
                W.row = 0;
                return W;
            } 
        }

        internal static Position EAST
        {
            get
            {
                Position E;
                E.col = 1;
                E.row = 0;
                return E;
            } 
        
        }

        internal static Position NORTH
        {
            get
            {
                Position N;
                N.col = 0;
                N.row = -1;
                return N;
            } 
        }
        internal static Position SOUTH {
            get
            {
                Position S;
                S.col = 0;
                S.row = -1;
                return S;
            } 
        }

        internal static Position EAST_TOP { 
            get {
                Position ET;
                ET.col = 2;
                ET.row = -1;
                return ET;
            } 
        }

        internal static Position EAST_BOTTOM
        {
            get
            {
                Position EB;
                EB.col = 2;
                EB.row = 1;
                return EB;
            }
        }

        internal static Position WEST_TOP
        {
            get
            {
                Position WT;
                WT.col = -2;
                WT.row = -1;
                return WT;
            }
        }

        internal static Position WEST_BOTTOM
        {
            get
            {
                Position WB;
                WB.col = -2;
                WB.row = 1;
                return WB;
            }
        }

        internal static Position NORTH_LEFT
        {
            get
            {
                Position NL;
                NL.col = -1;
                NL.row = -2;
                return NL;
            }
        }

        internal static Position NORTH_RIGHT
        {
            get
            {
                Position NR;
                NR.col = 1;
                NR.row = -2;
                return NR;
            }
        }

        internal static Position SOUTH_LEFT
        {
            get
            {
                Position SL;
                SL.col = -1;
                SL.row = 2;
                return SL;
            }
        }

        internal static Position SOUTH_RIGHT
        {
            get
            {
                Position SR;
                SR.col = 1;
                SR.row = 2;
                return SR;
            }
        }
    }
}

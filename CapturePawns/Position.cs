﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapturePawns
{
    public struct Position
    {
        public int row;


        public int col;
        

        public Position(int r, int c)
        {
            this.row = r;
            this.col = c;
        }

        public Position(Position value)
        {
            this.row = value.row;
            this.col = value.col;
        }

        public static bool operator ==(Position p1, Position p2)
        {
            return p1.row == p2.row && p1.col == p2.col;
        }

        public static bool operator !=(Position p1, Position p2)
        {
            return p1.row != p2.row || p1.col != p2.col;
        }

        public static bool operator >(Position p1, Position p2)
        {
            return p1.row > p2.row && p1.col > p2.col;
        }

        public static bool operator <(Position p1, Position p2)
        {
            return !(p1 > p2);
        }

        public static bool operator >=(Position p1, Position p2)
        {
            return p1 > p2 || p1 == p2;
        }

        public static bool operator <=(Position p1, Position p2)
        {
            return p1 < p2 || p1 == p2;
        }
        public override string ToString()
        {
            return this.col.ToString() + "," + this.row.ToString();
        }
         
    }

}
